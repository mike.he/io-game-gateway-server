<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use LaravelDoctrine\ORM\Facades\EntityManager;

class ScientistRepository extends EntityRepository
{
    public function getAll() {
        $query = $this->createQueryBuilder('s')
            ->select('s')
            ->leftJoin(
                'App\Entities\Theory',
                't',
                'WITH',
                't.scientist = s'
            )
            ->where('t.title = :title')
            ->setParameter('title', 'Theory')
            ->getQuery();

        $result = $query->getResult();

        return $result;
    }
}
