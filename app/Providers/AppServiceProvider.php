<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use App\Services\DockerService;
use App\Services\GatewayWorkerService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // JSON OBJECT Serialize Service
        $this->app->bind('serializer', function () {
            return new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
        });

        // Docker service
        $this->app->bind('docker_service', DockerService::class);

        // GatewayWorker service
        $this->app->bind('gateway_service', GatewayWorkerService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
