<?php

namespace App\Services;

use GatewayWorker\Lib\Gateway;

class GatewayWorkerService
{
    public function clientRegister($client_id, $uid) {
        Gateway::bindUid($client_id, $uid);
    }

    public function sendToUid(
        $client_id,
        $uid,
        $msg
    ) {
        $msgJson = json_encode([
            'from_uid' => Gateway::getUidByClientId($client_id),
            'body' => $msg,
        ], JSON_UNESCAPED_UNICODE);
        Gateway::sendToUid($uid, $msgJson);
    }

    public function joinGroup(
        $client_id,
        $group
    ) {
        Gateway::joinGroup($client_id, $group);
    }

    public function sendToGroup(
        $client_id,
        $group,
        $msg
    ) {
        $msgJson = json_encode([
            'from_group' => $group,
            'body' => $msg,
        ], JSON_UNESCAPED_UNICODE);

        Gateway::sendToGroup($group, $msgJson, $client_id);
    }

    public function textMsgSend(
        $client_id,
        $msg
    ) {
        Gateway::sendToClient($client_id, $msg);
    }

    public function xtermRenderSend(
        $client_id,
        $uid,
        $msg
    ) {
        Gateway::sendToUid($uid, $msg);
    }
}