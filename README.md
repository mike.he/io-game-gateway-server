# Doc

#### Client register
```json
{
	"channel": "gateway_service",
	"method": "clientRegister",
	"params": ["{{ uid }}"]
}
```

#### Join group
```json
{
	"channel": "gateway_service",
	"method": "joinGroup",
	"params": ["{{ group_id }}"]
}
```

#### Send message to group
> Sender
```json
{
	"channel": "gateway_service",
	"method": "sendToGroup",
	"params": [
        "{{ group_id }}",
        "{{ msg }}"
    ]
}
```

> Recipient
```json
{
    "from_group": "# from_group #",
    "body": "# msg #"
}
```

#### Send message to uid
    - params
        - to_uid: recipient ID
        - msg: message

> Sender
```json
{
	"channel": "gateway_service",
	"method": "sendToUid",
	"params": [
        "{{ to_uid }}",
        "{{ msg }}"
    ]
}
```

> Recipient
```json
{
    "from_uid": "# from_uid #",
    "body": "# msg #"
}
```