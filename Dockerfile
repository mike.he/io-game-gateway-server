FROM registry.cn-shanghai.aliyuncs.com/mikehe/mos:base

WORKDIR /workspace

# composer install
ADD ./composer.json /workspace/composer.json
ADD ./composer.lock /workspace/composer.lock

ADD . /workspace/

RUN composer install

ADD ./docker/entrypoint.sh /

RUN chmod +x /entrypoint.sh

EXPOSE 2345

ENTRYPOINT ["/entrypoint.sh"]
