- A Player发送指令并移动
	- A Player与B Player延时为300+200=500ms
	- A Player与C Player延时为300+150=450ms
```mermaid
sequenceDiagram
    A Player->>Game Server: {"player":"A","x":"+1","y":"0"}(delay:300ms)
    Game Server->>B Player: {"player":"A","x":"+1","y":"0"}(delay:200ms)
    Game Server->>C Player: {"player":"A","x":"+1","y":"0"}(delay:150ms)
```
---
- A Player发送指令不移动，A Player收到Game Server指令移动
	- A Player与B Player延时为300-200=100ms
	- A Player与C Player延时为300-150=150ms
```mermaid
sequenceDiagram
    A Player->>Game Server: {"player":"A","x":"+1","y":"0"}(delay:300ms)
    Game Server->>A Player: {"player":"A","x":"+1","y":"0"}(delay:300ms)
	loop handleMove
        A Player->A Player: {"x":"+1","y":"0"}
    end
    Game Server->>B Player: {"player":"A","x":"+1","y":"0"}(delay:200ms)
    Game Server->>C Player: {"player":"A","x":"+1","y":"0"}(delay:150ms)
```