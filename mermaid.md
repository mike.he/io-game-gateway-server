```mermaid
gantt
    dateFormat  YYYY-MM-DD-HH:ii
    title Adding GANTT diagram functionality to mermaid
    section Mike的任务
    起床洗漱            :done,    des1, 2019-05-18 09:00,15m
    吃早餐               :active,  des2,after des1, 20m
    午饭椒盐虾               :         des3, after des2, 5m
    晚饭排骨汤               :         des4, after des3, 5m
```

```mermaid
sequenceDiagram
Alice ->> Bob: Hello Bob, how are you?
Bob-->>John: How about you John?
Bob--x Alice: I am good thanks!
Bob-x John: I am good thanks!
Note right of John: Bob thinks a long<br/>long time, so long<br/>that the text does<br/>not fit on a row.

Bob-->Alice: Checking with John...
Alice->John: Yes... John, how are you?
```